import React from "react";
import { Hero } from "./partial/Hero.jsx";
import {WhatSpecial} from "./partial/WhatSpecial";
import { Games } from "./partial/Games";
import {SystemRequirement} from "./partial/SystemRequirement.jsx";
import { Newsletter } from "./partial/Newsletter.jsx";


const Index = () => {

  return <React.Fragment>
    
      <Hero/>
      <WhatSpecial/>
      <Games/>
      <SystemRequirement/>
      <Newsletter/>
      
  </React.Fragment>
};

export default Index;