import React, {
  useEffect,
  useContext,
  useState,
  useCallback,
  useRef,
} from "react";
import axios from "../../api/axios";
import { useParams } from "react-router-dom";
import AuthContext from "../../store/auth-context";
import rock from "../../images/batu.png";
import scissors from "../../images/gunting.png";
import paper from "../../images/kertas.png";
import refresh from "../../images/refresh.png";
import ReactDom from "react-dom";

const Modal = ({ visible, close, winner, startInterval }) => {
  return (
    <div
      id='modal-result'
      onClick={close}
      className={`fixed inset-0 bg-white/5 backdrop-blur-sm  flex justify-start z-30 ${
        visible ? "translate-y-0" : "translate-y-full"
      } duration-300`}>
      <div className=' absolute w-full'>
        <div className='grid grid-rows-[10em_10em] place-content-center  h-screen place-items-center gap-20'>
          <div>
            <div className='px-1 py-24  bg-green-700 rotate-45 rounded-2xl inline-flex justify-center items-center'>
              <h1 className='-rotate-90 text-white text-[2rem] leading-none flex justify-center '>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  fill='none'
                  viewBox='0 0 24 24'
                  strokeWidth={1.5}
                  stroke='currentColor'
                  className='w-12 h-full text-white'>
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    d='M16.5 18.75h-9m9 0a3 3 0 013 3h-15a3 3 0 013-3m9 0v-3.375c0-.621-.503-1.125-1.125-1.125h-.871M7.5 18.75v-3.375c0-.621.504-1.125 1.125-1.125h.872m5.007 0H9.497m5.007 0a7.454 7.454 0 01-.982-3.172M9.497 14.25a7.454 7.454 0 00.981-3.172M5.25 4.236c-.982.143-1.954.317-2.916.52A6.003 6.003 0 007.73 9.728M5.25 4.236V4.5c0 2.108.966 3.99 2.48 5.228M5.25 4.236V2.721C7.456 2.41 9.71 2.25 12 2.25c2.291 0 4.545.16 6.75.47v1.516M7.73 9.728a6.726 6.726 0 002.748 1.35m8.272-6.842V4.5c0 2.108-.966 3.99-2.48 5.228m2.48-5.492a46.32 46.32 0 012.916.52 6.003 6.003 0 01-5.395 4.972m0 0a6.726 6.726 0 01-2.749 1.35m0 0a6.772 6.772 0 01-3.044 0'
                  />
                </svg>{" "}
                {winner}
              </h1>
            </div>
          </div>
          <div>
            <button
              className='w-32 h-32'
              id='refresh'
              onClick={startInterval}>
              <img
                src={refresh}
                alt='refresh'
                className='inline-block'
              />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

const Index = (props) => {
  const { id } = useParams();
  const authCtx = useContext(AuthContext);

  const [showModal, setShowModal] = useState(false);
  const onCloseSidebarHandler = () => setShowModal(false);
  const [games, setGames] = useState(["rock", "paper", "scissors"]);
  const [selectedCom, setSelectedCom] = useState("");
  const [intervalId, setIntervalId] = useState(0);
  const [matchResult, setMatchResult] = useState("");

  const comRock = useRef();
  const comPaper = useRef();
  const comScissors = useRef();

  const randomComHandler = useCallback(() => {
    let random = Math.floor(Math.random() * games.length);
    setSelectedCom(games[random]);
  }, [games]);

  useEffect(() => {
    let intervalId = setInterval(randomComHandler, 1000);
    setIntervalId(intervalId);
    return () => {
      clearInterval(intervalId);
    };
  }, [randomComHandler]);

  useEffect(() => {
    if (matchResult) {
      const data = {
        user_id: authCtx.user.id,
        game_id: id,
        score:
          matchResult === "player 1 win" ? 3 : matchResult === "draw" ? 0 : -1,
      };
      const postData = async () => {
        console.log("post");
        await axios.post("/game-history/add", data, {
          headers: {
            Authorization: authCtx.token,
          },
        });
      };

      postData();
      setShowModal(true);
    }
  }, [matchResult, authCtx.user.id, id, authCtx.token]);

  const playerHandler = (val) => {
    const selectedPlayer = games[val];
    let matchResult = null;

    if (selectedCom === "rock") comRock.current.focus();
    else if (selectedCom === "paper") comPaper.current.focus();
    else if (selectedCom === "scissors") comScissors.current.focus();

    if (selectedPlayer === selectedCom) matchResult = `draw`;
    else if (selectedPlayer === "rock" && selectedCom === "scissors")
      matchResult = `player 1 win`;
    else if (selectedPlayer === "scissors" && selectedCom === "paper")
      matchResult = `player 1 win`;
    else if (selectedPlayer === "paper" && selectedCom === "rock")
      matchResult = `player 1 win`;
    else matchResult = `com win`;

    setMatchResult(matchResult);

    if (intervalId) {
      clearInterval(intervalId);
      setIntervalId(0);
    }
  };

  const onStartIntervalHandler = () => {
    const newIntervalId = setInterval(randomComHandler, 1000);
    setIntervalId(newIntervalId);
    setShowModal(false);
    setMatchResult("");
  };

  return (
    <React.Fragment>
      <section className='container mx-auto my-auto flex flex-col lg:flex-row justify-evenly items-center gap-2 lg:gap-3 h-screen'>
        <div className='flex flex-col justify-center items-center'>
          <h1 className='tracking-wider text-[3rem]'>Player</h1>
          <div className='inline-flex flex-row lg:flex-col'>
            <button
              className='p-choice group hover:outline hover:outline-green/50 rounded-full focus:outline focus:outline-green/50'
              data-value='rock'
              onClick={() => playerHandler(0)}>
              <img
                src={rock}
                alt='batu'
                className='w-40 h-40  px-8 py-8'
              />
            </button>
            <button
              className='p-choice group hover:outline hover:outline-green/50 rounded-full focus:outline focus:outline-green/50'
              data-value='paper'
              onClick={() => playerHandler(1)}>
              <img
                src={paper}
                alt='paper'
                className='w-40 h-40 px-8 py-8'
              />
            </button>
            <button
              className='p-choice group hover:outline hover:outline-green/50 rounded-full focus:outline focus:outline-green/50'
              data-value='scissors'
              onClick={() => playerHandler(2)}>
              <img
                src={scissors}
                alt='scissors'
                className='w-40 h-40  px-8 py-8'
              />
            </button>
          </div>
        </div>
        <div className='flex flex-col justify-center items-center'>
          <div id='match-section'>
            <h1 className='text-red-800 text-[5em] lg:text-[15em]'>VS</h1>
          </div>
        </div>
        <div className='flex flex-col justify-center items-center'>
          <h1 className='tracking-wider text-[3rem]'>Computer</h1>
          <div className='inline-flex flex-row lg:flex-col'>
            <button
              className='com-choice group hover:outline hover:outline-green/50 rounded-full focus:outline focus:outline-green/50'
              data-value='rock'
              ref={comRock}>
              <img
                src={rock}
                alt='batu'
                className='w-40 h-40 px-8 py-8'
              />
            </button>
            <button
              className='com-choice group hover:outline hover:outline-green/50 rounded-full focus:outline focus:outline-green/50'
              data-value='paper'
              ref={comPaper}>
              <img
                src={paper}
                alt='paper'
                className='w-40 h-40  px-8 py-8'
              />
            </button>
            <button
              className='com-choice group hover:outline hover:outline-green/50 rounded-full focus:outline focus:outline-green/50'
              data-value='scissors'
              ref={comScissors}>
              <img
                src={scissors}
                alt='scissors'
                className='w-40 h-40  px-8 py-8'
              />
            </button>
          </div>
        </div>
      </section>

      {ReactDom.createPortal(
        <Modal
          close={onCloseSidebarHandler}
          visible={showModal}
          winner={matchResult}
          startInterval={onStartIntervalHandler}
        />,
        document.getElementById("root_modal"),
      )}
    </React.Fragment>
  );
};

export default Index;
