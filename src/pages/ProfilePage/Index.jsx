/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unreachable */
import React, { useContext, useEffect, useState } from "react";
import profileImg from "../../images/profile.jpg";
import AuthContext from "../../store/auth-context";
import axios from "../../api/axios";
import Moment from "moment";

import { ref, uploadBytes, getDownloadURL, listAll, list } from "firebase/storage";
import { storage } from "../../firebase";


const Index = () => {
  const autCtx = useContext(AuthContext);
  const [userBiodata, setUserBiodata] = useState({});
  const [gameHistory, setGameHistory] = useState([]);
  const [userStats, setUserStats] = useState({});
  const [email, setEmail] = useState(autCtx?.user?.email);
  const [password, setPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [isChanged, setIsChanged] = useState(true);
  const [isHistoryChanged, setIsHistoryChanged] = useState(true);

  const [imageUpload, setImageUpload] = useState(null);
  const [imageUrls, setImageUrls] = useState([]);

  const logoutHandler = () => {
    localStorage.clear();
    window.location.reload();
  };

  const imageListRef = ref(storage, "Files/");
  const uploadFile = () => {
    if (imageUpload == null) return;
    const imageRef = ref(storage, `Files/${imageUpload.name}`);
    uploadBytes(imageRef, imageUpload).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        setImageUrls((prev) => [...prev], url)
      })
    })
  }

  const updateProfileHandler = async (e) => {

    e.preventDefault();

    const data = {
      email,
      password,
      firstName,
      lastName,
      phoneNumber,
    };

    await axios.post(`/users/update/${autCtx.user.id}`, data, {
      headers: {
        Authorization: autCtx.token,
      },
    });

    setIsChanged(true);
    console.log(isChanged);
  };

  useEffect(() => {

    listAll(imageListRef).then((response) => {
      response.items.forEach((item) => {
        getDownloadURL(item).then((url) => {
          setImageUrls((prev) => [...prev, url])
        })
      })
    })

    if (isChanged) {
      const fetchDataProfile = async () => {
        try {
          const responses = await axios.get(`/users/detail/${autCtx.user.id}`, {
            headers: {
              Authorization: autCtx.token,
            },
          });

          if (responses.status === 200) {
            const data = responses.data.user;

            setPhoneNumber(data.phoneNumber);
            setLastName(data.lastName);
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setPhoneNumber(data.phoneNumber);
            setUserBiodata(data);
          }
        } catch (error) {
          console.log(error);
        }
      };
      fetchDataProfile();
    }

    if (isHistoryChanged) {
      const fetchDataHistory = async () => {
        try {
          const responses = await axios.get(
            `/game-history/history-user-game/${autCtx.user.id}`,
            {
              headers: {
                Authorization: autCtx.token,
              },
            },
          );

          if (responses.status === 200) {
            const data = responses.data;
            setGameHistory(data);
          }
        } catch (error) {
          console.log(error);
        }
      };

      const fecthUserStats = async () => {
        try {
          const responses = await axios.get(
            `/game-history/stats/${autCtx.user.id}`,
            {
              headers: {
                Authorization: autCtx.token,
              },
            },
          );

          if (responses.status === 200) {
            const data = responses.data;
            setUserStats(data);
          }
        } catch (error) {
          console.log(error);
        }
      };

      fetchDataHistory();
      fecthUserStats();
    }

    return () => {
      setIsChanged(false);
      setIsHistoryChanged(false);
    };


  }, [autCtx.user.id, autCtx.token, isChanged, isHistoryChanged]);

  return (
    <div className='mt-20 flex flex-wrap items-center justify-center'>

      <div className='container max-w-lg bg-white rounded shadow-lg transform duration-200 easy-in-out lg:m-12 m-6'>
        <div className='h-2/4 sm:h-64 overflow-hidden'>
          <img
            className='w-full rounded-t object-cover'
            src={profileImg}
            alt={"background profile"}
          />
        </div>
        <div className='flex justify-start px-5 -mt-12 mb-5'>
          <span clspanss='block relative h-32 w-32'>
            {/* <img
              alt={"Photo by aldi sigun on Unsplash"}
              src='https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fHByb2ZpbGUlMjBwaWN0dXJlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60'
              className='mx-auto object-cover rounded-full h-24 w-24 bg-white p-1'
            /> */}
            {imageUrls.map((url,index) => {
                return <img className='mx-auto object-cover rounded-full h-24 w-24 bg-white p-1' key={index} src={url} /> 
            })} 
          </span>
        </div>
        <div className='px-7 mb-8'>
          <h2 className='text-3xl font-bold text-purple-700'>
            {userBiodata?.firstName} {userBiodata?.lastName}
          </h2>
        </div>
      </div>

      <div className='grid grid-rows-1 lg:grid-cols-[20em_1fr] max-w-lg container m-6 gap-6'>
        <div className='flex flex-col gap-3 row-start-2 lg:row-start-1 space-y-4'>
          <div className='flex-1 bg-white shadow-lg grid'>
            <h1 className='font-semibold text-lg lg:text-2xl p-4 rounded-lg'>
              Stats
            </h1>
            <div className='flex p-3 gap-5'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                strokeWidth={1.5}
                stroke='currentColor'
                className='w-12 h-full text-slate-500'>
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M21 12a9 9 0 11-18 0 9 9 0 0118 0z'
                />
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M15.91 11.672a.375.375 0 010 .656l-5.603 3.113a.375.375 0 01-.557-.328V8.887c0-.286.307-.466.557-.327l5.603 3.112z'
                />
              </svg>
              <div>
                <h3 className='font-normal text-slate-400'>Game Played</h3>
                <p className='font-bold'>{userStats?.gamePlayed}</p>
              </div>
            </div>
            <div className='flex p-3 gap-5'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                strokeWidth={1.5}
                stroke='currentColor'
                className='w-12 h-full text-slate-500'>
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M16.5 18.75h-9m9 0a3 3 0 013 3h-15a3 3 0 013-3m9 0v-3.375c0-.621-.503-1.125-1.125-1.125h-.871M7.5 18.75v-3.375c0-.621.504-1.125 1.125-1.125h.872m5.007 0H9.497m5.007 0a7.454 7.454 0 01-.982-3.172M9.497 14.25a7.454 7.454 0 00.981-3.172M5.25 4.236c-.982.143-1.954.317-2.916.52A6.003 6.003 0 007.73 9.728M5.25 4.236V4.5c0 2.108.966 3.99 2.48 5.228M5.25 4.236V2.721C7.456 2.41 9.71 2.25 12 2.25c2.291 0 4.545.16 6.75.47v1.516M7.73 9.728a6.726 6.726 0 002.748 1.35m8.272-6.842V4.5c0 2.108-.966 3.99-2.48 5.228m2.48-5.492a46.32 46.32 0 012.916.52 6.003 6.003 0 01-5.395 4.972m0 0a6.726 6.726 0 01-2.749 1.35m0 0a6.772 6.772 0 01-3.044 0'
                />
              </svg>

              <div>
                <h3 className='font-normal text-slate-400'>Total Win</h3>
                <p className='font-bold'>{userStats?.totalWin}</p>
              </div>
            </div>
            <div className='flex p-3 gap-5'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                strokeWidth={1.5}
                stroke='currentColor'
                className='w-12 h-full text-slate-500'>
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M10.05 4.575a1.575 1.575 0 10-3.15 0v3m3.15-3v-1.5a1.575 1.575 0 013.15 0v1.5m-3.15 0l.075 5.925m3.075.75V4.575m0 0a1.575 1.575 0 013.15 0V15M6.9 7.575a1.575 1.575 0 10-3.15 0v8.175a6.75 6.75 0 006.75 6.75h2.018a5.25 5.25 0 003.712-1.538l1.732-1.732a5.25 5.25 0 001.538-3.712l.003-2.024a.668.668 0 01.198-.471 1.575 1.575 0 10-2.228-2.228 3.818 3.818 0 00-1.12 2.687M6.9 7.575V12m6.27 4.318A4.49 4.49 0 0116.35 15m.002 0h-.002'
                />
              </svg>

              <div>
                <h3 className='font-normal text-slate-400'>Total Draw</h3>
                <p className='font-bold'>{userStats?.totalDraw}</p>
              </div>
            </div>
            <div className='flex p-3 gap-5'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                strokeWidth={1.5}
                stroke='currentColor'
                className='w-12 h-full text-slate-500'>
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M7.5 15h2.25m8.024-9.75c.011.05.028.1.052.148.591 1.2.924 2.55.924 3.977a8.96 8.96 0 01-.999 4.125m.023-8.25c-.076-.365.183-.75.575-.75h.908c.889 0 1.713.518 1.972 1.368.339 1.11.521 2.287.521 3.507 0 1.553-.295 3.036-.831 4.398C20.613 14.547 19.833 15 19 15h-1.053c-.472 0-.745-.556-.5-.96a8.95 8.95 0 00.303-.54m.023-8.25H16.48a4.5 4.5 0 01-1.423-.23l-3.114-1.04a4.5 4.5 0 00-1.423-.23H6.504c-.618 0-1.217.247-1.605.729A11.95 11.95 0 002.25 12c0 .434.023.863.068 1.285C2.427 14.306 3.346 15 4.372 15h3.126c.618 0 .991.724.725 1.282A7.471 7.471 0 007.5 19.5a2.25 2.25 0 002.25 2.25.75.75 0 00.75-.75v-.633c0-.573.11-1.14.322-1.672.304-.76.93-1.33 1.653-1.715a9.04 9.04 0 002.86-2.4c.498-.634 1.226-1.08 2.032-1.08h.384'
                />
              </svg>

              <div>
                <h3 className='font-normal text-slate-400'>Total Lose</h3>
                <p className='font-bold'>{userStats?.totalLose}</p>
              </div>
            </div>
          </div>

          <div className='bg-white shadow-lg rounded-lg space-y-3 overflow-hidden'>
            <h1 className='font-semibold text-lg lg:text-2xl p-4'>
              History Game
            </h1>

            <div className='overflow-auto relative sm:rounded-lg h-64'>
              <table className='w-full text-sm text-left text-gray-500'>
                <thead className='text-xs text-gray-700 uppercase bg-gray-50 '>
                  <tr>
                    <th
                      scope='col'
                      className='py-3 px-6'>
                      Game Name
                    </th>
                    <th
                      scope='col'
                      className='py-3 px-6'>
                      Game Play Date
                    </th>
                    <th
                      scope='col'
                      className='py-3 px-6'>
                      Game Result
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {gameHistory.map((data) => {
                    return (
                      <tr
                        className='bg-white border-b'
                        key={data.id}>
                        <th
                          scope='row'
                          className='py-4 px-6 font-medium text-gray-900 whitespace-nowrap'>
                          {data.Games?.name}
                        </th>
                        <td className='py-4 px-6'>
                          {Moment(data.date).format("DD/MM/YY")}
                        </td>
                        <td className='py-4 px-6'>
                          {data.score === 3
                            ? "Win"
                            : data.score === 2
                              ? "Draw"
                              : data.score === 1
                                ? "Lose"
                                : null}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>

          <button
            className='flex-row inline-flex gap-3 rounded-full bg-red-600 items-center justify-center cursor-pointer py-3 px-4 text-white transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300'
            onClick={logoutHandler}>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              fill='none'
              viewBox='0 0 24 24'
              strokeWidth={1.5}
              stroke='currentColor'
              className='w-6 h-6'>
              <path
                strokeLinecap='round'
                strokeLinejoin='round'
                d='M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75'
              />
            </svg>
            Log out
          </button>
        </div>

        <div>
          <div className='bg-white shadow-lg rounded-lg p-4 space-y-10 overflow-hidden'>
            <h1 className='font-semibold text-lg lg:text-2xl'>User Profile</h1>

            <form onSubmit={updateProfileHandler} onClick={uploadFile}>
              <div className='relative z-0 mb-6 w-full group'>
                <input
                  type='email'
                  name='floating_email'
                  id='floating_email'
                  className='block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-purple-600 peer'
                  placeholder=' '
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
                <label
                  htmlFor='floating_email'
                  className='peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-purple-600 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6'>
                  Email address
                </label>
              </div>
              <div className='relative z-0 mb-6 w-full group'>
                <input
                  type='password'
                  name='floating_password'
                  id='floating_password'
                  className='block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-purple-600 peer'
                  placeholder=' '
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <label
                  htmlFor='floating_password'
                  className='peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-purple-600  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6'>
                  Password
                </label>
              </div>
              <div className='grid md:grid-cols-2 md:gap-6'>
                <div className='relative z-0 mb-6 w-full group'>
                  <input
                    type='text'
                    name='floating_first_name'
                    id='floating_first_name'
                    className='block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-purple-600 peer'
                    placeholder=' '
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                  />
                  <label
                    htmlFor='floating_first_name'
                    className='peer-focus:font-medium absolute text-sm text-gray-500  duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-purple-600  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6'>
                    First name
                  </label>
                </div>
                <div className='relative z-0 mb-6 w-full group'>
                  <input
                    type='text'
                    name='floating_last_name'
                    id='floating_last_name'
                    className='block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none  focus:outline-none focus:ring-0 focus:border-purple-600 peer'
                    placeholder=' '
                    required
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                  <label
                    htmlFor='floating_last_name'
                    className='peer-focus:font-medium absolute text-sm text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-purple-600  peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6'>
                    Last name
                  </label>
                </div>
              </div>
              <div className='grid md:grid-cols-2 md:gap-6'>
                <div className='relative z-0 mb-6 w-full group'>
                  <input
                    type='tel'
                    pattern='\+?([ -]?\d+)+|\(\d+\)([ -]\d+)'
                    name='floating_phone'
                    id='floating_phone'
                    className='block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-purple-600 peer'
                    placeholder=' '
                    required
                    value={phoneNumber}
                    onChange={(e) => setPhoneNumber(e.target.value)}
                  />
                  <label
                    htmlFor='floating_phone'
                    className='peer-focus:font-medium absolute text-sm text-gray-500  duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-purple-600 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6'>
                    Phone number (+62-8126-309123)
                  </label>
                </div>
                <div className='relative z-0 mb-6 w-full group'>
                  <label
                    htmlFor='formFile'
                    className='form-label inline-block mb-2 text-purple-700'>
                    Photo
                  </label>
                  <input
                    className='form-control block w-full px-3 py-1.5 text-base font-normal text-purple-700  bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-purple-700 focus:bg-white focus:border-purple-600 focus:outline-none'
                    type='file'
                    id='formFile'
                    onChange={(event) => {
                      setImageUpload(event.target.files[0]);
                    }}
                  />
                </div>

              </div>
              <button className='bg-purple-700 text-white hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center'>
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;
