import React, { useEffect, useState } from "react";
import image from "../../images/rps.jpg";
import { useParams } from "react-router-dom";
import axios from "../../api/axios";
import { Link } from "react-router-dom";

const Index = () => {
  const { id } = useParams();
  const [game, setGame] = useState({});
  useEffect(() => {
    const getDetail = async () => {
      try {
        const response = await axios.get(`games/detail/${id}`);
        setGame(response.data);
      } catch (error) {
        console.log(error, "error");
      }
    };

    getDetail();
  }, [id]);

  return (
    <section className='mt-24 mx-auto  w-11/12'>
      <div className='grid grid-rows-1 lg:grid-cols-2 gap-4'>
        <div className='h-96 rounded-2xl'>
          <img
            src={game.image}
            alt='image1'
            className='object-cover w-full rounded-2xl'
          />
        </div>
        <div className='bg-[#F7FAFC] p-6 rounded-2xl'>
          <h1 className='font-extrabold text-2xl lg:text-3xl tracking-wide'>
            {game.name}
          </h1>
          <h3 className='font-bold text-lg mt-5'>About Game</h3>
          <p className='text-slate-600'>{game.description}</p>
          {!game.isPlayable ? (
            <button
              className='px-8 py-3  text-white bg-gray-300 border-none rounded-full w-full mt-24'
              disabled='disabled'>
              Under Maintenance
            </button>
          ) : (
            <Link
              to={`/room/${game.id}`}
              className='btn btn-primary bg-[#7189FF] border-none rounded-full w-full mt-24'>
              Play
            </Link>
          )}
        </div>
      </div>
    </section>
  );
};

export default Index;
