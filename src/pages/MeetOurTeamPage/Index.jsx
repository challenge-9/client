import React from 'react'
import { Hero } from "./partial/Hero";
import { TeamMember } from './partial/TeamMember';

const Index = () => {
  return (
   <React.Fragment>
     <Hero/>
     <TeamMember/>
   </React.Fragment>
  )
}

export default Index