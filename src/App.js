import "./App.css";
import React from "react";
import { Routes, Route } from "react-router-dom";
import Layout from "./components/Layout";
import LayoutNonNavbar from "./components/LayoutNonNavbar";
import ProtectedRoute from "./components/ProtectedRoute";

const LandingPage = React.lazy(() => import("./pages/LandingPage/Index"));
const LoginPage = React.lazy(() => import("./pages/LoginPage/Index"));
const RegisterPage = React.lazy(() => import("./pages/RegisterPage/Index"));
const GamePage = React.lazy(() => import("./pages/GamePage/Index"));
const RoomGamePage = React.lazy(() => import("./pages/RoomGamePage/Index"));
const GameDetailPage = React.lazy(() => import("./pages/GameDetailPage/Index"));
const AccountProfilePage = React.lazy(() =>
  import("./pages/ProfilePage/Index"),
);

const MeetOurTeamPage = React.lazy(() =>
  import("./pages/MeetOurTeamPage/Index"),
);
function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route
          path="/"
          element={
            <React.Suspense fallback={<p>Please wait</p>}>
              <LandingPage />
            </React.Suspense>
          }
        />

        <Route
          path="/meet-out-team"
          element={
            <React.Suspense fallback={<p>Please wait</p>}>
              <MeetOurTeamPage />
            </React.Suspense>
          }
        />

        <Route
          path="/game"
          element={
            <React.Suspense fallback={<p>Please wait</p>}>
              <GamePage />
            </React.Suspense>
          }
        />

        <Route element={<ProtectedRoute />}>
          <Route
            path="/account-profile/:id"
            element={
              <React.Suspense fallback={<p>Please wait</p>}>
                <AccountProfilePage />
              </React.Suspense>
            }
          />
          <Route
            path="/room/:id"
            element={
              <React.Suspense fallback={<p>Please wait</p>}>
                <RoomGamePage />
              </React.Suspense>
            }
          />
        </Route>

        <Route
          path="/game/:id"
          element={
            <React.Suspense fallback={<p>Please wait</p>}>
              <GameDetailPage />
            </React.Suspense>
          }
        />
      </Route>

      <Route path="/login" element={<LayoutNonNavbar />}>
        <Route
          path="/login"
          element={
            <React.Suspense fallback={<p>Please wait</p>}>
              <LoginPage />
            </React.Suspense>
          }
        />
      </Route>
      <Route path="/register" element={<LayoutNonNavbar />}>
        <Route
          path="/register"
          element={
            <React.Suspense fallback={<p>Please wait</p>}>
              <RegisterPage />
            </React.Suspense>
          }
        />
      </Route>
    </Routes>
  );
}

export default App;
