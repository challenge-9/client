import { Outlet } from "react-router-dom";
import { Navigation } from "./Navigation";
import { Footer } from "./Footer";
import React from "react";

const Layout = () => {
  return (
    <React.Fragment>
      <Navigation />
      <Outlet />
      <Footer />
    </React.Fragment>
  );
};

export default Layout;
