import { useLocation, Navigate, Outlet } from "react-router-dom";
import AuthContext from "../store/auth-context";
import { useContext } from "react";

const ProtectedRoute = () => {
  const authCtx = useContext(AuthContext);
  const isLogin = authCtx.isLogin;
  const location = useLocation();

  return isLogin ? (
    <Outlet />
  ) : (
    <Navigate
      to='/login'
      state={{ from: location }}
      replace
    />
  );
};

export default ProtectedRoute;
