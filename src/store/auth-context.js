import React, { useState, useCallback } from "react";
var CryptoJS = require("crypto-js");

const AuthContext = React.createContext({
  user: {},
  token: "",
  isLoggedIn: false,
  addToken: (token) => {},
  removeToken: () => {},
});

const retrieveStoredToken = () => {
  const storedToken = localStorage.getItem("token");

  let decryptedData;
  if (storedToken) {
    const bytes = CryptoJS.AES.decrypt(storedToken, "sdiu123n23oiu13@123");
    decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  } else {
    decryptedData = null;
  }

  return {
    user: decryptedData?.user,
    token: decryptedData?.token,
    storedToken: storedToken,
  };
};

export const AuthContextProvider = (props) => {
  const tokenData = retrieveStoredToken();

  let initialToken;
  let user;
  if (tokenData) {
    initialToken = tokenData.token;
    user = tokenData.user;
  }

  const [token, setToken] = useState(initialToken);

  const isLogin = token ? true : false;

  const addTokenHandler = useCallback(({ token, user }) => {
    const data = { user, token };

    var cipherText = CryptoJS.AES.encrypt(
      JSON.stringify(data),
      "sdiu123n23oiu13@123",
    ).toString();

    setToken(token);

    localStorage.setItem("token", cipherText);
  }, []);

  const removeTokenHandler = useCallback(() => {
    setToken(null);
    localStorage.removeItem("token");
  }, []);

  const contextValue = {
    user: user,
    token: token,
    isLogin: isLogin,
    addToken: addTokenHandler,
    removeToken: removeTokenHandler,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
